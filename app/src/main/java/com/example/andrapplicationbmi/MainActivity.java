package com.example.andrapplicationbmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    Button button;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button2);
        TextInputEditText editTextCC  =  (TextInputEditText)   findViewById(R.id.textviewHeight);
        TextInputEditText editTextCN  =  (TextInputEditText)  findViewById(R.id.textviewWeight);
        TextView textViewBMI =  (TextView)  findViewById(R.id.textView8);
        TextView textViewDG  =  (TextView) findViewById(R.id.textView10);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewBMI.setText("");
                textViewDG.setText("");
                if (editTextCC.getText().toString().isEmpty() && editTextCN.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this, "Thiếu dữ liệu chiều cao và cân nặng", Toast.LENGTH_LONG).show();
                    editTextCC.requestFocus();
                } else if (editTextCC.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Thiếu dữ liệu chiều cao", Toast.LENGTH_LONG).show();
                    editTextCC.requestFocus();
                } else if (editTextCN.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Thiếu dữ liệu cân nặng", Toast.LENGTH_LONG).show();
                    editTextCN.requestFocus();
                } else {
                    double CC           =   Double.parseDouble(editTextCC.getText()+"");
                    double CN           =   Double.parseDouble(editTextCN.getText()+"");
                    DecimalFormat dcf   =   new DecimalFormat("0.00");
                    double BMI          =   CN / Math.pow(CC, 2) * 10000;
                    if ((CC == 0) || (CN ==  0)) {
                        Toast.makeText(MainActivity.this, "Chiều cao và cân nặng phải khác 0", Toast.LENGTH_LONG).show();
                    }
                    else {
                        textViewBMI.setText("Chỉ số BMI của bạn là: " + dcf.format(BMI));
                        if (BMI < 18) {
                            textViewDG.setText("Bạn quá gầy, cần bổ sung thêm nhiều chất dinh dưỡng hơn nữa nhé");
                        } else if (18 <= BMI && BMI < 24.9) {
                            textViewDG.setText("Body chuẩn nhé bạn, cứ tiếp tục phát huy!");
                        } else if (25 <= BMI && BMI < 29.9) {
                            textViewDG.setText("Bạn đang béo phì cấp độ I, cần có chế độ giảm cân hợp lý");
                        } else if (30 <= BMI && BMI < 34.9) {
                            textViewDG.setText("Bạn đang béo phì cấp độ II, cần có chế độ giảm cân hợp lý");
                        } else {
                            textViewDG.setText("Bạn đang béo phì cấp độ III, cần có chế độ giảm cân hợp lý");
                        }
                    }
                }
            }
        });
    }
}